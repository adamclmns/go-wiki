package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

type Page struct {
	Title string
	Body  []byte
}

func (p *Page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	filename := title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil
}

func main() {
	p1 := &Page{Title: "Test Page", Body: []byte("This is a sample test page.")}
	p1.save()
	p2, err := loadPage("Test Page")
	if err != nil {
		fmt.Printf("ERROR OCCURRED")
	}
	fmt.Println(string(p2.Body))
	// Calling Handler in main
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8082", nil)
}

//HTTP Handler code
func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hi there, I love %s!", r.URL.Path[1:])
}
